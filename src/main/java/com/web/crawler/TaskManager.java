package com.web.crawler;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UncheckedIOException;
import java.util.Properties;

public class TaskManager {

	private Properties prop = new Properties();
	private PageRepo pagesRepo = new PageRepo();
	private ProcessingQueue processingQu = new ProcessingQueue();
	private ParsingQueue parsingQu = new ParsingQueue();
	private PrintStream out;

	public TaskManager(PrintStream out) {
		this.out = out;
		try {
			Thread currentThread = Thread.currentThread();
			ClassLoader contextClassLoader = currentThread.getContextClassLoader();
			InputStream input = contextClassLoader.getResourceAsStream("config.properties");
			prop.load(input);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}

	}

	public void submit(String url) {
		if(url.isEmpty()) {
			out.println("Please provide a valid URL");
			return;
		}
		out.println("Crawling");
		processingQu.create(new Page(url, 0));
		new Crawler(processingQu, pagesRepo, parsingQu).run();
		new Parser(processingQu, pagesRepo, parsingQu, new Filter(url,prop)).run();

	}


}
