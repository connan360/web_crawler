package com.web.crawler;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PageRepo {

	private Map<String,Page> hm = new ConcurrentHashMap<String,Page>();
	
	public void create(Page page) {

		if(page == null) {
			throw new IllegalArgumentException("page object is empty");
		}
		
		hm.put(page.getUrl(), page);
	}
	
	public Page get(String url) {
		
		if(url.trim().isEmpty()) {
			throw new IllegalArgumentException("url is empty");
		}
		
		return hm.get(url);
	}
	
	public boolean contains(String url) {

		if(url.trim().isEmpty()) {
			throw new IllegalArgumentException("url is empty");
		}
		
		return hm.containsKey(url);
	}
	
	public Map<String,Page> getAll() {
		return hm;
	}
	

}
