package com.web.crawler;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;



public class ProcessingQueue {

	private Queue<Page> q = new ConcurrentLinkedQueue<>();
	private Map<String,Page> hm = new ConcurrentHashMap<String,Page>();
	
	public void create(Page page) {
		if(page == null) {
			throw new IllegalArgumentException("page object is null");
		}
		q.add(page);
		hm.put(page.getUrl(), page);
	}

	public Page getFirst() {
		
		return q.poll();
	}
	
	public Boolean contains(String childURL) {
		return hm.containsKey(childURL);
	}
	
	public int getQueueSize() {
		return q.size();
	}

}
