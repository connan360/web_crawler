package com.web.crawler;


import java.util.concurrent.atomic.AtomicInteger;

public class Page {

	private  int id;
	private static  AtomicInteger id_counter = new AtomicInteger();
	private String url;
	private int parentId ;
	
	
	public Page(String url, Integer parentId) {
		this.url = url;
		this.parentId = parentId;
		this.id = id_counter.incrementAndGet();
	}


	public int getId() {
		return id;
	}

	public String getUrl() {
		return url;
	}
	

}
