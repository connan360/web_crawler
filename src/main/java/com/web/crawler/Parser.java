package com.web.crawler;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Parser implements Runnable {

	private ExecutorService exec = Executors.newFixedThreadPool(1);

	private PageRepo pages;
	private ProcessingQueue processingQu;
	private ParsingQueue parsingQu;
	private Filter filter;

	public Parser(ProcessingQueue processingQu, PageRepo pagesRepo, 
			ParsingQueue parsingQu, Filter filter) {

		this.pages = pagesRepo;
		this.processingQu = processingQu;
		this.parsingQu = parsingQu;
		this.filter = filter;

	}

	public Elements parseHtml(Document document) throws IOException {

		Elements linksOnPage = new Elements();

		if (document == null) {
			return null;
		}

		linksOnPage = document.select("a[href]");

		return linksOnPage;

	}

	public void createChildPages(Elements linksOnPage, Page pageObject) {

		for (Element page : linksOnPage) {

			String childURL = filter.URL(page.attr("abs:href"));

			if (childURL.isEmpty() || pages.contains(childURL) 
					|| processingQu.contains(childURL)) {
				continue;
			}

			Page childPageObject = new Page(childURL, pageObject.getId());
			processingQu.create(childPageObject);
			System.out.println("	Child URL: " + childPageObject.getUrl());

		}

	}


	public void process() {
		Document doc;
		Link link;
		Elements childLinks = null;
		int id = 0;

		while (id < 10) {

			link = parsingQu.getFirst();
			
			if (link == null) {
				
				try {
					id++;
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				continue;
			}

			id = 0;
			
			doc = link.getCurrentDocument();
			
			String validURL = filter.URL(doc.location());

			if (validURL.isEmpty() || pages.contains(validURL)) {
				continue;
			}
			
			Page pageObject = new Page(validURL, link.getId());
			pages.create(pageObject);

			try {
				childLinks = this.parseHtml(doc);
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			}
			
			if (childLinks == null) {
				continue;
			}


			System.out.println("Parent URL : " + pageObject.getUrl());
			createChildPages(childLinks, pageObject);
			
		}
		
		System.out.println("Timeout: Nothing to parse. Parser service to shutdown");
		exec.shutdown();
	}

	@Override
	public void run() {
		//Execute multiple times for multiple threads
		exec.execute(this::process);
	}

}
