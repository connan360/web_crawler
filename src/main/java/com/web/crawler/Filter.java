package com.web.crawler;

import java.net.URI;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Filter {
	
	private Properties properties;
	private String rootURL;
	private String pattern;
	
	public Filter(String rootURL,Properties prop) {
		this.properties = prop;
		this.rootURL = rootURL;
		this.pattern = getURLPattern();
	}
	
	
	public String URL(String url) {
		
		Matcher matcher = createMatcher(url, pattern);
		boolean m = matcher.find();
		
		if (!m) {
			return "";
		}

		return matcher.group();
	}

	private String getURLPattern() {

		URI uri = URI.create(rootURL);
		String hostname = uri.getHost();
		String pattern = String.format(properties.getProperty("pattern.matcher"), hostname);
		return pattern;
	}

	private Matcher createMatcher(String url, String pattern) {

		Pattern p = Pattern.compile(pattern);
		Matcher matcher = p.matcher(url);
		return matcher;
	}
}
