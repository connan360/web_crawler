package com.web.crawler;

import org.jsoup.nodes.Document;

public class Link {

	private Document currentDocument;
	private int id;

	public Link(Document currentDocument, int id) {
		this.currentDocument = currentDocument;
		this.id = id;
	}

	public Document getCurrentDocument() {
		return currentDocument;
	}


	public int getId() {
		return id;
	}


}
