package com.web.crawler;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ParsingQueue {

	private Queue<Link> q = new ConcurrentLinkedQueue<>();
	

	public void create(Link link) {
		if (link == null) {
			throw new IllegalArgumentException("link object is null");
		}
		q.add(link);
	}

	public Link getFirst() {

		return q.poll();
	}

	public Link peek() {

		return q.peek();
	}
	
	public int getQueueSize() {
		return q.size();
	}
}
