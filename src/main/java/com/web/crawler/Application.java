package com.web.crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class Application {

	static PrintStream out = System.out;
	
	public static void main(String[] args) throws IOException {

		TaskManager tm = new TaskManager(out);
		
		try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
			out.println("Please enter a URL and press enter: ");
			tm.submit(br.readLine());
		}
				
	}

}
