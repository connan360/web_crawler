package com.web.crawler;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Crawler implements Runnable {

	private ProcessingQueue processingQu;

	private PageRepo pagesRepo;

	private ParsingQueue parsingQueue;

	private ExecutorService exec = Executors.newFixedThreadPool(1);
	

	public Crawler(ProcessingQueue processingQu, PageRepo pagesRepo, ParsingQueue parsingQueue) {

		this.parsingQueue = parsingQueue;
		this.processingQu = processingQu;
		this.pagesRepo = pagesRepo;
	}
	
	public Connection.Response returnResponse(String url) throws IOException{
		return Jsoup.connect(url).execute();
	}

	public Document getPage(Page page) {

		String url = page.getUrl();
		Document document;
		Connection.Response response = null;

		if (url.isEmpty()) {
			return null;
		}
		

		try {
			
			response = returnResponse(url);
			
		}catch(Exception e) {
			e.printStackTrace();
			pagesRepo.create(page);
			return null;
		}
		
		
		try {
	
			document = response.parse();

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		return document;
	}

	public void process() {
		Page page;

		int counter = 0;
		
		while (counter < 10) {
			page = processingQu.getFirst();

			if (page == null) {
				try {
					counter++;
					Thread.sleep(1000);
				} catch (Exception e) {
					System.out.println(e);
				}
				continue;
			}

			counter = 0;
			
			if (pagesRepo.contains(page.getUrl())) {
				continue;
			}

			Document currentDocument = this.getPage(page);

			if (currentDocument == null) {
				continue;
			}

			parsingQueue.create(new Link(currentDocument,page.getId()));
		}
		System.out.println("Timeout: No URLs to crawl. Crawler service to shutdown");
		exec.shutdown();
	}

	@Override
	public void run() {
		exec.execute(this::process);
	}
}
