package com.web.crawler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PageRepoTest {

	private PageRepo pr;
	private Page p1;
	private Page p2;
	private Page p3;
	private Page p4;

	
	@Before
    public void setUp() {
		pr = new PageRepo();
		p1 = new Page("https://monzo.com", 0);
		p2 = new Page("https://bbc.co.uk",  1);
		p3 = new Page("https://google.com", 2);
		p4 = new Page("https://bbc.co.uk/sports", 1);
		
		pr.create(p1);
		pr.create(p2);
		pr.create(p3);
	}
	
	@Test
	public void create() {

		Assert.assertEquals(p1 , pr.get(p1.getUrl()));
		Assert.assertEquals(p2, pr.get(p2.getUrl()));
		Assert.assertEquals(p3, pr.get(p3.getUrl()));
	}
	

	@Test
	public void get() {
	
		assertEquals(p2,pr.get(p2.getUrl()));
		assertNull(pr.get(p4.getUrl()));
	}
	
	@Test
	public void contains() {
	
		assertEquals(true,pr.contains(p3.getUrl()));
		assertEquals(false,pr.contains(p4.getUrl()));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void invalidCreate() {
		pr.create(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void invalidGetParameter() {
		pr.get("");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void invalidContainsParameter() {
		pr.contains("");
	}
	
	@Test
	public void getAll() {
		assertEquals(3,pr.getAll().size());
	}

}
