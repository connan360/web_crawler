package com.web.crawler;

import static org.junit.Assert.assertNull;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class ProcessingQueueTest {
	
	private ProcessingQueue pr;

	private Page p1;
	private Page p2;
	private Page p3;

	
	@Before
    public void setUp() {
		pr = new ProcessingQueue();
		p1 = new Page("https://monzo.com", 0);
		p2 = new Page("https://bbc.co.uk",  1);
		p3 = new Page("https://google.com", 2);

		
		pr.create(p1);
		pr.create(p2);
		pr.create(p3);
	}

	@Test
	public void create() {
		
		Assert.assertEquals(p1.getUrl() , pr.getFirst().getUrl());
		Assert.assertEquals(p2.getUrl(), pr.getFirst().getUrl());
		Assert.assertEquals(p3.getUrl(), pr.getFirst().getUrl());
	}

	@Test
	public void getFirst() {
		Assert.assertEquals(p1.getUrl() , pr.getFirst().getUrl());
	}
	
	@Test
	public void emptyQueue() {
		Assert.assertEquals(p1.getUrl() , pr.getFirst().getUrl());
		Assert.assertEquals(p2.getUrl(), pr.getFirst().getUrl());
		Assert.assertEquals(p3.getUrl(), pr.getFirst().getUrl());
		assertNull(pr.getFirst());
	}
	

	@Test(expected = IllegalArgumentException.class)
	public void invalidCreate() {
		pr.create(null);
	}
	
	@Test
	public void contains() {
		
		Assert.assertEquals(true , pr.contains(p1.getUrl()));
		Assert.assertEquals(false ,pr.contains("https://false.com"));
	}

}
