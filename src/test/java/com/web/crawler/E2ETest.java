package com.web.crawler;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

public class E2ETest {

	

	@Test
	public void emptyUrlString() {
		ByteArrayOutputStream by = new ByteArrayOutputStream();
		PrintStream outContent = new PrintStream(by);
		
		
		TaskManager tm = new TaskManager(outContent);
		tm.submit("");
		String s = new String(by.toByteArray());
		
		assertEquals("Please provide a valid URL\n", s);
	}
	
}
