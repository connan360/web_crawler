package com.web.crawler;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ParsingQueueTest {
	
	private ParsingQueue pq;
	Document document = mock(Document.class);
	Link link1 = mock(Link.class);
	Link link2 = mock(Link.class);

	
	@Before
    public void setUp() {
		pq = new ParsingQueue();
		pq.create(link1);
		pq.create(link2);
	}
	
	@Test
	public void create() {
		when(link1.getId()).thenReturn(1);
		when(link2.getId()).thenReturn(2);
		
		Assert.assertEquals(1 , pq.getFirst().getId());
		Assert.assertEquals(2, pq.getFirst().getId());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void emptyLinkAsArgument() {
		Link link3 = null;
		pq.create(link3);
	}
	
	@Test
	public void peek() {
		when(link1.getId()).thenReturn(1);
		when(link2.getId()).thenReturn(2);
		
		Assert.assertEquals(1 , pq.peek().getId());
		Assert.assertEquals(1, pq.peek().getId());
	}
}
