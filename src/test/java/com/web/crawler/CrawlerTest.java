package com.web.crawler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;


import static org.mockito.Mockito.*;

public class CrawlerTest {

	private Crawler crawler;
	
	private ProcessingQueue processingQu = mock(ProcessingQueue.class);;
	private PageRepo pagesRepo = mock(PageRepo.class);
	private ParsingQueue parsingQueue = mock(ParsingQueue.class);
	
	Page page = mock(Page.class);
	
	@Before
	public void setup() {
		
		crawler = new Crawler(processingQu,pagesRepo, parsingQueue);
	}
	
	@Test
	public void getRequestWithValidURL() throws IOException {
		when(page.getUrl()).thenReturn("https://www.crawler-test.com");
		Document actualDocument = crawler.getPage(page);
		Document expectedDocument = Jsoup.connect(page.getUrl()).get();
		
		assertEquals(actualDocument.baseUri(),expectedDocument.baseUri());
	}
	
	@Test
	public void getRequestWithEmptyURLString() throws IOException {
		Page page = new Page("",0);
		Document document = crawler.getPage(page);
		
		assertNull(document);
	}
	
	@Test
	public void getRequestWithNotOKHttpRequest() throws IOException {
		Page page = new Page("https://www.crawler-test.com/random",0);
		assertNull(crawler.getPage(page));
		verify(pagesRepo).create(page);
	}
	
	@Test
	public void responseIOError() throws IOException {
		Page page = new Page("https://www",0);
		Crawler crawlerSpy = spy(crawler);
		Response response = mock(Response.class);
		
		doReturn(response).when(crawlerSpy).returnResponse(page.getUrl());
		when(response.parse()).thenThrow(new IOException());
		assertNull(crawlerSpy.getPage(page));
	}
	
	@Test
	public void process() {
		
		ProcessingQueue processingQu_1 = new ProcessingQueue();
		PageRepo pagesRepo_1 = new PageRepo();
		ParsingQueue parsingQueue_1 = new ParsingQueue();
		
		Page p1 = new Page("https://www.babylonhealth.com", 0);
		Page p2 = new Page("https://www.babylonhealth.com/about",  1);
		Page p3 = new Page("https://www.babylonhealth.com/blog", 1);
		Page p4 = new Page("https://www.babylonhealth.com/blog", 1);
		Page p5 = new Page("", 0);;
		
		processingQu_1.create(p1);
		processingQu_1.create(p2);
		processingQu_1.create(p3);
		processingQu_1.create(p5);
		
		pagesRepo_1.create(p4);
		
		Crawler crawler_process = new Crawler(processingQu_1,pagesRepo_1, parsingQueue_1);
		crawler_process.run();
		
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println(e);
		}
		
		assertEquals(0, processingQu_1.getQueueSize());
		assertEquals(2, parsingQueue_1.getQueueSize());
	}
}
