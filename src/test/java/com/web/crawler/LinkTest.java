package com.web.crawler;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;

public class LinkTest {

	private Document document = mock(Document.class);
	private Link link;
	int id = 0;
	
	@Before
	public void setup() {
		link = new Link(document,id);
	}
	
	@Test
	public void getCurrentDocument() {
		when(document.baseUri()).thenReturn("https://www.crawler-test.com");
		
		assertEquals("https://www.crawler-test.com", 
				link.getCurrentDocument().baseUri());
	}
	
	@Test
	public void getID() {
		assertEquals(0, 
				link.getId());
	}
}
