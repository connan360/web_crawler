package com.web.crawler;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class FilterTest {
	
	private Filter filter; 
	private Properties properties = new Properties();
	private String rootURL = "https://www.babylonhealth.com";
	
	@Before
	public void setup() {
		
		Thread currentThread = Thread.currentThread();
		ClassLoader contextClassLoader = currentThread.getContextClassLoader();
		InputStream input = contextClassLoader.getResourceAsStream("config.properties");
		try {
			properties.load(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		filter = new Filter(rootURL,properties);
	}
	
	@Test
	public void getURLPattern() {
		assertEquals("https://www.babylonhealth.com/blog", filter.URL("https://www.babylonhealth.com/blog/"));
	}
	
	@Test
	public void invalidURLPattern() {

		assertEquals("", filter.URL("https://apple.com/"));
	}
}
