package com.web.crawler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;

public class ParserTest {

	private Parser parser;

	private ProcessingQueue processingQu =  new ProcessingQueue();
	private PageRepo pagesRepo = mock(PageRepo.class);
	private ParsingQueue parsingQueue = new ParsingQueue();
	private Properties properties = new Properties();
	private String rootURL = "https://www.babylonhealth.com";
	private Document document = mock(Document.class);

	Page page = mock(Page.class);

	@Before
	public void setup() {

		Thread currentThread = Thread.currentThread();
		ClassLoader contextClassLoader = currentThread.getContextClassLoader();
		InputStream input = contextClassLoader.getResourceAsStream("config.properties");
		try {
			properties.load(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		parser = new Parser(processingQu, pagesRepo, parsingQueue, new Filter(rootURL, properties));
	}
	
	@Test
	public void parseHtml() throws IOException {
		Document doc = Jsoup.connect("http://jsoup.org").get();
		Elements  expectedLinksOnPage = doc.select("a[href]");
		
		assertEquals(expectedLinksOnPage, parser.parseHtml(doc));
		
		doc = null;
		assertNull(parser.parseHtml(doc));
	}
	
	@Test
	public void createChildPages() throws IOException {
		Document doc = Jsoup.connect(rootURL).get();
		Elements linksOnPage = parser.parseHtml(doc);
		parser.createChildPages(linksOnPage, new Page(rootURL,0));
		
		assertEquals(31, processingQu.getQueueSize());
	}
	
	@Test
	public void process() {
		
		ProcessingQueue processingQu_1 = new ProcessingQueue();
		PageRepo pagesRepo_1 = new PageRepo();
		ParsingQueue parsingQueue_1 = new ParsingQueue();
		
		Crawler crawler = new Crawler(processingQu_1,pagesRepo_1,parsingQueue_1);
		
		
		Page p1 = new Page("https://www.babylonhealth.com/about", 0);
		Page p2 = new Page("https://www.babylonhealth.com/product",  1);
		Page p3 = new Page("https://www.babylonhealth.com/blog", 1);
		Page p4 = new Page("https://www.babylonhealth.com/blog", 1);
		Page p5 = new Page("https://apple.com", 1);
		
		
		pagesRepo_1.create(p4);
		
		Document currentDocument_1 = crawler.getPage(p1);
		Document currentDocument_2 = crawler.getPage(p2);
		Document currentDocument_3 = crawler.getPage(p3);
		Document currentDocument_4 = crawler.getPage(p5);
		
		parsingQueue_1.create(new Link(currentDocument_1,p1.getId()));
		parsingQueue_1.create(new Link(currentDocument_2,p2.getId()));
		parsingQueue_1.create(new Link(currentDocument_3,p3.getId()));
		parsingQueue_1.create(new Link(currentDocument_4,p5.getId()));
		
		
		Parser parser_process = new Parser(processingQu_1,pagesRepo_1, parsingQueue_1,new Filter(rootURL, properties));
		parser_process.run();
		
		try {
			Thread.sleep(2000);
		} catch (Exception e) {
			System.out.println(e);
		}
		
		assertEquals(0, parsingQueue_1.getQueueSize());
		assertEquals(3, pagesRepo_1.getAll().size());
		
	}

}
